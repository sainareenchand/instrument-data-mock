package com.instrumentdatamock.sample;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class Application {

    public static void main(String[] args) throws URISyntaxException, IOException {

        String privateKey = args[0] + "private.pem.key";
        String deviceCert = args[0] + "device-certificate.pem.crt";
        String rootCa = args[0] + "AmazonRootCA1.pem";
        String topic = args[1]; // AWS MQTT topic name
        String endpoint = args[2]; // AWS IOT Core Data endpoint.

        // Get locations from .json file in resources
        ObjectMapper mapper = new ObjectMapper();
        List<Location> clockWiseLocations = mapper.readValue(Paths.get(ClassLoader.getSystemResource("clockwise.json")
                .toURI())
                .toFile(), new TypeReference<>() {});

        Instrument ventisPro1 = new Instrument();
        ventisPro1.setSerialNumber("VPRO-INS-1");

        Instrument ventisPro2 = new Instrument();
        ventisPro2.setSerialNumber("VPRO-INS-2");

        InstrumentPublish instrumentPublish1 = new InstrumentPublish();
        instrumentPublish1.setInstrument(ventisPro1);
        instrumentPublish1.setClientId("VPRO-INS-1-" + UUID.randomUUID().toString());
        instrumentPublish1.setEndpoint(endpoint);
        instrumentPublish1.setLocations(clockWiseLocations);
        instrumentPublish1.setPrivateKeyPath(privateKey);
        instrumentPublish1.setDeviceCertificatePath(deviceCert);
        instrumentPublish1.setRootCaPath(rootCa);
        instrumentPublish1.setTopic(topic);

        Thread thread1 = new Thread(instrumentPublish1);
        thread1.start();

        List<Location> antiClockWiseLocations = new ArrayList<>(clockWiseLocations);
        Collections.reverse(antiClockWiseLocations);

        InstrumentPublish instrumentPublish2 = new InstrumentPublish();
        instrumentPublish2.setInstrument(ventisPro2);
        instrumentPublish2.setClientId("VPRO-INS-2-" + UUID.randomUUID().toString());
        instrumentPublish2.setEndpoint(endpoint);
        instrumentPublish2.setLocations(antiClockWiseLocations);
        instrumentPublish2.setPrivateKeyPath(privateKey);
        instrumentPublish2.setDeviceCertificatePath(deviceCert);
        instrumentPublish2.setRootCaPath(rootCa);
        instrumentPublish2.setTopic(topic);

        Thread thread2 = new Thread(instrumentPublish2);
        thread2.start();
    }
}
