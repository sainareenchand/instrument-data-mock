package com.instrumentdatamock.sample;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import software.amazon.awssdk.crt.CRT;
import software.amazon.awssdk.crt.io.ClientBootstrap;
import software.amazon.awssdk.crt.io.EventLoopGroup;
import software.amazon.awssdk.crt.io.HostResolver;
import software.amazon.awssdk.crt.mqtt.MqttClientConnection;
import software.amazon.awssdk.crt.mqtt.MqttClientConnectionEvents;
import software.amazon.awssdk.crt.mqtt.MqttMessage;
import software.amazon.awssdk.crt.mqtt.QualityOfService;
import software.amazon.awssdk.iot.AwsIotMqttConnectionBuilder;

import java.time.Instant;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Getter
@Setter
@NoArgsConstructor
public class InstrumentPublish implements Runnable {

    private Instrument instrument;
    private List<Location> locations;

    private String clientId;
    private String endpoint;
    private String topic;
    private String rootCaPath;
    private String deviceCertificatePath;
    private String privateKeyPath;


    @Override
    public void run() {
        publishToIoTCore();
    }

    public void publishToIoTCore() {
        ObjectMapper mapper = new ObjectMapper();

        try (EventLoopGroup eventLoopGroup = new EventLoopGroup(1);
             HostResolver resolver = new HostResolver(eventLoopGroup);
             ClientBootstrap clientBootstrap = new ClientBootstrap(eventLoopGroup, resolver);
             AwsIotMqttConnectionBuilder mqttConnectionBuilder = AwsIotMqttConnectionBuilder.newMtlsBuilderFromPath(deviceCertificatePath, privateKeyPath)) {

            MqttClientConnectionEvents callbacks = new MqttClientConnectionEvents() {
                @Override
                public void onConnectionInterrupted(int errorCode) {
                    if (errorCode != 0) {
                        System.out.println("Connection interrupted: " + errorCode + ": " + CRT.awsErrorString(errorCode));
                    }
                }

                @Override
                public void onConnectionResumed(boolean sessionPresent) {
                    System.out.println("Connection resumed: " + (sessionPresent ? "existing session" : "clean session"));
                }
            };

            mqttConnectionBuilder.withBootstrap(clientBootstrap)
                    .withCertificateAuthorityFromPath(null, rootCaPath)
                    .withConnectionEventCallbacks(callbacks)
                    .withClientId(clientId)
                    .withEndpoint(endpoint)
                    .withCleanSession(true);

            try (MqttClientConnection connection = mqttConnectionBuilder.build()) {

                // Connect to MQTT Broker
                CompletableFuture<Boolean> connected = connection.connect();
                try {
                    boolean sessionPresent = connected.get();
                    System.out.println("Connected to " + (!sessionPresent ? "new" : "existing") + " session!");
                } catch (Exception ex) {
                    throw new RuntimeException("Exception occurred during connect", ex);
                }

                // Publish to topic.
                for (int index = 0; index <= locations.size(); index++) {

                    instrument.setId(java.util.UUID.randomUUID().toString());
                    instrument.setCoReading(String.valueOf(Math.random()));
                    instrument.setH2SReading(String.valueOf(Math.random()));
                    instrument.setLocation(locations.get(index));
                    instrument.setRecUpdateTime(Instant.now().toString());
                    String payload = mapper.writeValueAsString(instrument);
                    MqttMessage payloadMqttMessage = new MqttMessage(topic, payload.getBytes());
                    System.out.println("Message Sent: "  + payload);
                    CompletableFuture<Integer> published = connection.publish(payloadMqttMessage, QualityOfService.AT_LEAST_ONCE, false);
                    published.get();
                    Thread.sleep(1000);
                }

                CompletableFuture<Void> disconnected = connection.disconnect();
                disconnected.get();

            } catch (Exception ex) {
                System.out.println("Error" + ex);
            }
        }
    }
}
