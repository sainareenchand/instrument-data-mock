package com.instrumentdatamock.sample;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Instrument {
    private String id;
    private String serialNumber;
    private Location location;
    private String coReading;
    private String h2SReading;
    private String recUpdateTime;
}
